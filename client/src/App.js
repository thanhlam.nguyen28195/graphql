import { useLazyQuery, useMutation } from "@apollo/react-hooks";
import { Button } from "@material-ui/core";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import MenuItem from "@material-ui/core/MenuItem";
import { withStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import MUIDataTable from "mui-datatables";
import React, { useState } from "react";
import { Spinner } from "reactstrap";
import CustomToolbar from "./components/CustomToolbar";
import { DELETE_USER, GET_USERS } from "./Queries";
const defaultToolbarStyles = {
  iconButton: {},
};
const currencies = [
  {
    value: "USD",
    label: "$",
  },
  {
    value: "EUR",
    label: "€",
  },
  {
    value: "BTC",
    label: "฿",
  },
  {
    value: "JPY",
    label: "¥",
  },
];
function App() {
  const columns = ["ID", "Name", "Job Title", "Email"];
  let dataTable = [];
  const [deleteUser] = useMutation(DELETE_USER);
  const [loadGreeting, { called, loading, data }] = useLazyQuery(GET_USERS);
  const [currency, setCurrency] = useState("EUR");
  const [open, setOpen] = useState(false);
  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleChange = (event) => {
    setCurrency(event.target.value);
  };
  const handleClose = () => {
    setOpen(false);
  };
  const options = {
    filterType: "dropdown",
    responsive: "scroll",
    onRowsDelete: (object) => {
      const idxDataDelete = Object.keys(object.lookup);
      idxDataDelete.map(async (dataDelete) => {
        await deleteUser({
          variables: { id: parseInt(dataTable[dataDelete][0]) },
        });
      });
    },
    onRowClick: (rowData, rowMeta) => {
      console.log("rowData", rowData);
      console.log("rowMeta", rowMeta);
      handleClickOpen();
    },
    customToolbar: () => {
      return <CustomToolbar handleClickOpen={handleClickOpen} />;
    },
  };

  if (data && data.getUsers) {
    data.getUsers.map((user) => {
      const parseUserToArray = [user.id, user.name, user.job_title, user.email];
      return dataTable.push(parseUserToArray);
    });
  }
  // const userInfo = useQuery(VIEW_USERS, { variables: { id: 1 } });
  if (called && loading) return <Spinner color="dark" />;
  // if (getAllUsers.error || userInfo.error)
  // return <React.Fragment>Error :(</React.Fragment>;
  return (
    <div className="container">
      <Button variant="outlined" color="primary" onClick={() => loadGreeting()}>
        Search
      </Button>
      <MUIDataTable
        title={"ACME Employee list"}
        data={dataTable}
        columns={columns}
        options={options}
      />
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">Add Employee</DialogTitle>
        <DialogContent>
          <DialogContentText>
            To subscribe to this website, please enter your email address here.
            We will send updates occasionally.
          </DialogContentText>
          <TextField
            autoFocus
            margin="dense"
            id="id"
            label="ID"
            fullWidth
            variant="outlined"
            InputProps={{
              readOnly: true,
            }}
          />
          <TextField
            autoFocus
            margin="dense"
            id="name"
            label="Name"
            fullWidth
            variant="outlined"
          />
          <TextField
            id="standard-select-currency"
            select
            label="Select"
            value={currency}
            onChange={handleChange}
            helperText="Please select your currency"
          >
            {currencies.map((option) => (
              <MenuItem key={option.value} value={option.value}>
                {option.label}
              </MenuItem>
            ))}
          </TextField>
          <TextField
            autoFocus
            margin="dense"
            id="name"
            label="Email Address"
            type="email"
            fullWidth
            variant="outlined"
          />
          
        </DialogContent>

        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Disagree
          </Button>
          <Button onClick={handleClose} color="primary" autoFocus>
            Agree
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}

export default withStyles(defaultToolbarStyles, { name: "App" })(App);
